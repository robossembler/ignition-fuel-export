# Ignition Fuel Exporter

Python package for publishing 3D models of parts and assemblies on [Ignition Fuel Server](https://gitlab.com/ignitionrobotics/web/fuelserver) for simulation

## Purpose

The key feature is to automate the export of models from CAD to Ignition Gazebo robotics simulator and help engineers to create robotic/non-robotic agents and run realistic simulations without programming.

Imagine how cool it would be if an engineer/designer could upload his version of the product and check it for assemblability, and a programmer or technologist could debug production control programs for models that were directly exported from engineers projects with git-like version control. No file transfers - everything works in the same ecosystem. You start the simulator, open the model registry there and add what you want. Great, isn't it?

## How it works

1. Parse the assembly in STEP format using [trimesh](https://github.com/mikedh/trimesh) library
2. Split the assembly into shapes/parts in the form of Shape objects
3. For each shape
	* Create a model.config and add the repository address with the hash of a specific commit, as well as the package version from the tag
	* Create a package with collision and visual models and an [SDF](http://sdformat.org/spec)-file. The same parts represented by the same package. The "sameness" of the parts is determined using geometry serialization
	* Publish on the server [Ignition Fuel](https://gitlab.com/ignitionrobotics/web/fuelserver) via [REST API](https://app.ignitionrobotics.org/api)
4. Create a package for Ignition Fuel for the initial assembly, where the parts specified using the URI of the parts created early

## Install

	docker-compose build

## To Do

In addition to geometrical properties from CAD simulator needs other data for high-quality simulation:
1. Material density for calculating inertial properties
2. Characteristics of the material for light reflection, including the selection of textures
3. Surface properties for different physical engines-elasticity, friction
4. Parameters of joints and sensors

To add this data we can:
1. Create an sdf file in XML format in repo, link texture-files and other additional data 
2. Create a plugin for FreeCAD/Solidworks, so that it is convenient to set the parameters of bodies in the graphical interface

## Usage
before up container:

	docker-compose up -d

Then run script, the script takes 2 parameters:

	make exec file_path="{full_file_path}" token={token}

the third optional model privacy flag: private=False or private=false for example: 

	make exec file_path="{full_file_path}" token={token} private=false

	