from abc import ABC, abstractmethod
from src.geometric_objects.abstract_geometry_object import AbstractGeometryObject


class AbstractReader(ABC):

    @abstractmethod
    def read_file(self, file_pass: str) -> AbstractGeometryObject:
        pass


