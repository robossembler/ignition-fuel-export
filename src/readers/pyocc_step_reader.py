from OCC.Core.TopoDS import TopoDS_Shape
from OCC.Extend.TopologyUtils import TopologyExplorer
from .abstract_reader import AbstractReader
from ..geometric_objects.abstract_geometry_object import AbstractGeometryObject
from OCC.Extend.DataExchange import read_step_file
from ..geometric_objects.pyocc_object import PyoccObject
# from OCC.TopExp import TopExp_Explorer

class PyoccStepReader(AbstractReader):

    def read_file(self, file_pass: str) -> AbstractGeometryObject:
        compound = read_step_file(file_pass)
        # display.EraseAll()
        # display.DisplayShape(compound, update=True)
        return PyoccObject(compound)
