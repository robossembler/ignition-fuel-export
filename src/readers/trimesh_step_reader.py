from trimesh import interfaces, Trimesh
from .abstract_reader import AbstractReader
from ..geometric_objects.abstract_geometry_object import AbstractGeometryObject
from ..geometric_objects.trimesh_object import TrimeshObject


class TrimeshStepReader(AbstractReader):

    def read_file(self, file_pass: str) -> AbstractGeometryObject:
        gmsh = interfaces.gmsh.load_gmsh(file_pass)
        return TrimeshObject(Trimesh(**gmsh))
