from __future__ import annotations
import os
from collections import Iterator
from OCC.Core import GProp, BRepGProp
from OCC.Core.TopoDS import TopoDS_Shape
from OCC.Extend.DataExchange import write_stl_file
from OCC.Extend.TopologyUtils import TopologyExplorer
from src.geometric_objects.abstract_geometry_object import AbstractGeometryObject


class PyoccObject(AbstractGeometryObject):

    def __init__(self, obj: TopoDS_Shape):
        super().__init__()
        self.__pyocc = obj # объект SwigPyObject

    def split(self) -> Iterator[AbstractGeometryObject]:
        t = TopologyExplorer(self.__pyocc)
        wrap = lambda x: PyoccObject(x)
        return map(wrap, t.solids())

    def prepare(self):
        pass

    def get_hash_identifier(self) -> str:
        return str(self.__pyocc.HashCode(12))

    def export_stl(self) -> bytes:
        file_path = './temp.stl'
        write_stl_file(self.__pyocc, file_path)
        with open(file_path) as f:
            pass
        os.remove(file_path)
        return bytes(f)

    def export_collada(self) -> bytes:
        geom = bytes()
        return geom

    def get_moment_inertia(self) -> dict:
        props = GProp.GProp_GProps()
        BRepGProp.brepgprop_LinearProperties(self.__pyocc, props)
        m = props.MatrixOfInertia()   # объект SwigPyObject
        s = str(m)
        return {
            'xx': self.__pyocc.moment_inertia[0][0],
            'yy': self.__pyocc.moment_inertia[1][1],
            'zz': self.__pyocc.moment_inertia[2][2],
            'xy': self.__pyocc.moment_inertia[0][1],
            'xz': self.__pyocc.moment_inertia[0][2],
            'yz': self.__pyocc.moment_inertia[1][2],
        }

    def get_mass(self) -> str:
        props = GProp.GProp_GProps()
        BRepGProp.brepgprop_SurfaceProperties(self.__pyocc, props)
        props.Mass() # объект SwigPyObject

        return ''



