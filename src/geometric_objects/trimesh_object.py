import hashlib
from collections import Iterable
from trimesh.exchange.stl import export_stl
from trimesh.exchange.dae import export_collada
from trimesh import Trimesh, interfaces
import numpy as np
from trimesh import util
from src.geometric_objects.abstract_geometry_object import AbstractGeometryObject


class TrimeshObject(AbstractGeometryObject):

    def __init__(self, obj: Trimesh):
        super().__init__()
        self.__obj = obj
        self.density = self.__obj.density

    def split(self) -> Iterable[AbstractGeometryObject]:
        details = self.__obj.split()
        wrap = lambda x: TrimeshObject(x)
        return map(wrap, details)

    def prepare(self):
        self.__convert_units_to_mm()

    def get_hash_identifier(self) -> str:
        arr_round = np.abs(np.round(self.__obj.identifier, 1))
        return hashlib.md5(
            util.array_to_encoded(arr_round, encoding='base64')['base64'].encode('utf-8')
        ).hexdigest()

    def export_stl(self) -> bytes:
        return export_stl(self.__obj)

    def export_collada(self) -> bytes:
        return export_collada(self.__obj)

    def get_moment_inertia(self) -> dict:
        return {
            'xx': self.__obj.moment_inertia[0][0],
            'yy': self.__obj.moment_inertia[1][1],
            'zz': self.__obj.moment_inertia[2][2],
            'xy': self.__obj.moment_inertia[0][1],
            'xz': self.__obj.moment_inertia[0][2],
            'yz': self.__obj.moment_inertia[1][2],
        }

    def get_mass(self) -> str:
        return str(self.__obj.mass)

    def __convert_units_to_mm(self):
        self.__obj.units = 'mm'
        self.__obj.convert_units('m')
