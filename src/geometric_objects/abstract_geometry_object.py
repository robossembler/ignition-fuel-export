from __future__ import annotations
from abc import ABC, abstractmethod
from collections import Iterable


class AbstractGeometryObject(ABC):

    def __init__(self):
        self.density: float = 1.0
        self.metadata: dict = {}

    @abstractmethod
    def split(self) -> Iterable[AbstractGeometryObject]:
        pass

    @abstractmethod
    def prepare(self):
        pass

    @abstractmethod
    def get_hash_identifier(self) -> str:
        pass

    @abstractmethod
    def export_stl(self) -> bytes:
        pass

    @abstractmethod
    def export_collada(self) -> bytes:
        pass

    '''
    return Dict:
        {
            'xx': {moment_inertia[xx]},
            'yy': {moment_inertia[yy]},
            'zz': {moment_inertia[zz]},
            'xy': {moment_inertia[xy]},
            'xz': {moment_inertia[xz]},
            'yz': {moment_inertia[yz]},
        }
    '''
    @abstractmethod
    def get_moment_inertia(self) -> dict:
        pass

    @abstractmethod
    def get_mass(self) -> str:
        pass
