from .data_converter import DataConverter
from .geometric_objects.abstract_geometry_object import AbstractGeometryObject
from .ignition_fuel_api import IgnitionFuelApi


class MeshHandler:

    def __init__(self, converter: DataConverter, api: IgnitionFuelApi):
        self.api = api
        self.converter = converter

    def handle_mesh(
            self,
            geometry_obj: AbstractGeometryObject,
            hash_assembly_unit: str,
            name_prefix: str,
            private_model_status: bool
    ) -> None:
        detail_identifier = geometry_obj.get_hash_identifier()
        model_name = f'{name_prefix}{detail_identifier}'
        geometry_obj.metadata.update(hash_assembly_unit=hash_assembly_unit)
        geometry_obj.metadata.update(hash_detail_identifier=detail_identifier)
        geometry_obj.metadata.update(model_name=model_name)
        prepare_data = self.converter.get_prepare_data(geometry_obj)
        self.api.create_model(prepare_data, model_name, private_model_status)
