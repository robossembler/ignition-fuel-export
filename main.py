import os
from sys import argv
from src.data_converter import DataConverter
from src.geometric_objects.abstract_geometry_object import AbstractGeometryObject
from src.ignition_fuel_api import IgnitionFuelApi
from src.git_helper import GitHelper
from src.mesh_handler import MeshHandler
from src.readers.abstract_reader import AbstractReader
from src.readers.trimesh_step_reader import TrimeshStepReader


def main(reader: AbstractReader, file_name: str, token: str, is_model_privat: bool = True) -> None:
    geometry_obj = reader.read_file(file_name)
    geometry_obj.prepare()
    hash_assembly_unit = geometry_obj.get_hash_identifier()
    mesh_hendler = MeshHandler(DataConverter(GitHelper()), IgnitionFuelApi(token))
    mesh_hendler.handle_mesh(geometry_obj, hash_assembly_unit, 'assembly_', is_model_privat)

    details = geometry_obj.split()
    for detail in details:
        detail: AbstractGeometryObject
        mesh_hendler.handle_mesh(detail, hash_assembly_unit, 'part_', is_model_privat)


def get_params(*args) -> dict:
    if len(args) < 3:
        raise ValueError('''
        must be min 2 parameters \n
        1 - full file path \n
        2 - private token \n
        3 - is privat model optional (0 or 1)
        ''')
    params = {'file_name': args[1], 'token': args[2]}
    if not os.path.exists(params['file_name']):
        raise ValueError('file is not exist')
    if len(args) > 3:
        is_model_privat = args[3].lower() != 'false'
        params['is_model_privat'] = is_model_privat
    return params


if __name__ == '__main__':
    params = get_params(*argv)
    main(TrimeshStepReader(), **params)
