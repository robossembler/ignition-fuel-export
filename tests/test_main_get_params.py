import os
from unittest import TestCase
from main import get_params


class TestMainGetParams(TestCase):

    def test_get_not_exist_file(self):
        with self.assertRaises(ValueError) as excinfo:
            get_params(*['script_name', 'file_path', 'any_token'])
        assert 'file is not exist' in str(excinfo.exception)

    def test_too_few_parameters(self):
        with self.assertRaises(ValueError) as excinfo:
            get_params(*['script_name', 'file_path'])
        assert '''
        must be min 2 parameters \n
        1 - full file path \n
        2 - private token \n
        3 - is privat model optional (0 or 1)
        ''' in str(excinfo.exception)

    def test_get_too_param(self):
        abspath = os.path.abspath(__file__)
        self.assertEqual(
            get_params(*['script_name', abspath, 'private_token']),
            {'file_name': abspath, 'token': 'private_token'}
        )

    param_list = [
        ('False', False),
        ('false', False),
        ('True', True),
        ('true', True),
    ]

    def test_private_model_flag(self):
        abspath = os.path.abspath(__file__)
        for actual, expected in self.param_list:
            with self.subTest():
                self.assertEqual(
                    get_params(*['script_name', abspath, 'private_token', actual]),
                    {'file_name': abspath, 'token': 'private_token', 'is_model_privat': expected}
                )

