file_name = $(shell basename $(file_path))

exec:
	docker cp $(file_path) trimesh:/usr/src/app/$(file_name)
	docker exec -it trimesh python main.py /usr/src/app/$(file_name) $(token) $(private)
	docker exec -it trimesh rm /usr/src/app/$(file_name)